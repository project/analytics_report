<?php

namespace Drupal\analytics_report_matomo\Plugin\AnalyticsReport;

use Drupal\analytics_report\Objects\AnalyticsDevices;
use Drupal\analytics_report\Objects\AnalyticsGraph;
use Drupal\analytics_report\Objects\AnalyticsLive;
use Drupal\analytics_report\Objects\AnalyticsOrigin;
use Drupal\analytics_report\Objects\AnalyticsPerPage;
use Drupal\analytics_report\Objects\AnalyticsSummary;
use Drupal\analytics_report\Plugin\AnalyticsReport\AnalyticsReportBase;
use Drupal\analytics_report_matomo\MatomoQueryFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides 'Matomo' analytics report.
 *
 * @AnalyticsReport(
 *   id = "matomo",
 *   name = @Translation("Matomo"),
 *   description = @Translation("Make sure the configuration for Matomo Analytics module is provided (/admin/config/system/matomo) and API key is added to settings.php before enabling this plugin."),
 * )
 */
class Matomo extends AnalyticsReportBase {

  /**
   * The name of the authentication token for the plugin.
   */
  const MATOMO_API_AUTHENTICATION_PARAMETER_NAME = 'analytics_report_matomo_api_key';

  /**
   * The Matomo query factory.
   *
   * @var \Drupal\analytics_report_matomo\MatomoQueryFactoryInterface
   */
  protected MatomoQueryFactoryInterface $matomoQueryFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    /** @var \Drupal\analytics_report_matomo\Plugin\AnalyticsReport\Matomo $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->pathAliasManager = $container->get('path_alias.manager');
    $instance->logger = $container->get('logger.factory')->get('analytics_report');
    $instance->matomoQueryFactory = $container->get('analytics_report_matomo.query_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredConfigAndSettings(): array {
    return [
      'config' => [
        'matomo.settings' => ['site_id', 'url_http', 'url_http'],
      ],
      'settings' => [self::MATOMO_API_AUTHENTICATION_PARAMETER_NAME],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDevices(?string $url = NULL): ?AnalyticsDevices {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:devices' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Set query parameters.
    $params = [
      'period' => 'year',
      'date' => 'today',
    ];

    // Add URL parameter if provided.
    if ($url) {
      $params['segment'] = urlencode('pageUrl==' . $url);
    }

    // Get devices data.
    $devices = $this->request('DevicesDetection', 'getType', $params);

    // Stop here if no data was found.
    if (!$devices) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsDevices();
    $supported_devices = ['desktop', 'smartphone', 'tablet'];
    foreach ($devices as $device) {
      if (in_array(mb_strtolower(trim($device->label)), $supported_devices, TRUE)) {
        $data->setDevice($device->label, $device->nb_visits);
      }
    }

    // Add the data to the cache. The device data can be cached for 24 hours.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (24 * 60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getGraph(string $period, ?string $url = NULL): ?AnalyticsGraph {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:graph:' . $period . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Set query parameters. Period can be day, week, month or year.
    $params = [
      'module' => 'VisitsSummary',
      'report' => 'getVisits',
      'period' => $period,
      'date' => 'last7',
    ];

    // Add URL parameter if provided.
    if ($url) {
      $params['segment'] = urlencode('pageUrl==' . $url);
    }

    // Get periodic data.
    $result = $this->request($params['module'], $params['report'], $params) ?? [];

    // Stop here if no data was found.
    if (!$result) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsGraph();
    $format = 'Y-m-d';
    foreach ($result as $date => $value) {
      // Based on the provided period the date can be a single date or a range
      // seperated by a comma. In case of a range the first date is used.
      if ('week' === $params['period']) {
        $dates = explode(',', $date);
        $date = array_shift($dates);
      }

      // Alter the format when the period is by month.
      if ('month' === $params['period']) {
        $format = 'Y-m';
      }

      // Alter the format when the period is by year.
      if ('year' === $params['period']) {
        $format = 'Y';
      }

      // Create DateTime object from date.
      $datetime = DrupalDateTime::createFromFormat($format, $date);
      $datetime->setTimezone(new \DateTimeZone('UTC'));
      $datetime->setTime(12, 0);
      $data->setDateVisitors($datetime->getTimestamp(), $value ?: 0);
    }

    // Add the data to the cache. The graph data can be cached for 24 hours.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (24 * 60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getLive(?string $url = NULL): ?AnalyticsLive {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:live' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Set query parameters.
    $params = [
      'module' => 'API',
      'lastMinutes' => 30,
    ];

    // Add URL parameter if provided.
    if ($url) {
      $params['segment'] = urlencode('pageUrl==' . $url);
    }

    // Get real time data.
    $live = $this->request('Live', 'getCounters', $params);

    // Stop here if no data was found.
    if (!$live) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsLive();
    $live = reset($live);
    $data->setActions($live->actions ?? 0);
    $data->setMinutes($params['lastMinutes']);
    $data->setVisitors($live->visitors ?? 0);

    // Add the data to the cache. The live data can be cached for 30 minutes.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (30 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrigin(?string $url = NULL): ?AnalyticsOrigin {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:origin' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Set query parameters.
    $params = [
      'period' => 'year',
      'date' => 'today',
      'filter_limit' => 6,
    ];

    // Add URL parameter if provided.
    if ($url) {
      $params['segment'] = urlencode('pageUrl==' . $url);
    }

    // Get origin data.
    $countries = $this->request('UserCountry', 'getCountry', $params);

    // Stop here if no data was found.
    if (!$countries) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsOrigin();
    foreach ($countries as $country) {
      $data->setCountry($country->label, $country->nb_visits);
    }

    // Add the data to the cache. The origin data can be cached for 1 hour.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(?string $url = NULL): ?AnalyticsSummary {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:summary' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Set query parameters.
    $params = [
      'period' => 'year',
      'date' => 'today',
    ];

    // Add URL parameter if provided.
    if ($url) {
      $params['segment'] = urlencode('pageUrl==' . $url);
    }

    // Get visits summary, visitors frequency data and actions summary.
    $visits_summary = $this->request('VisitsSummary', 'get', $params);
    $visit_frequency = $this->request('VisitFrequency', 'get', $params);
    $actions_summary = $this->request('Actions', 'get', $params);

    // Stop here if no data was found.
    if (!$visits_summary && !$visit_frequency && !$actions_summary) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsSummary();
    if ($visits_summary) {
      $data->setBounceRate($visits_summary->bounce_rate ?? '0%');
      $data->setVisitAvgTime($visits_summary->avg_time_on_site ?? 0);
      $data->setVisitors($visits_summary->nb_visits ?? 0);
      $data->setPageviewsPerVisit($visits_summary->nb_actions_per_visit ?? 0);
    }
    if ($visit_frequency) {
      // Only available in 'day' reports.
      $data->setVisitorsNew($visit_frequency->nb_visits_new ?? 0);
    }
    if ($actions_summary) {
      $data->setPageviews($actions_summary->nb_pageviews ?? 0);
    }

    // Add the data to the cache. The summary data can be cached for 1 hour.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageViews(string $period): array {
    // Set query parameters. Matomo needs the url to be encoded.
    $params = [
      'period' => $period,
      'date' => 'today',
      'showColumns' => 'nb_visits',
      'filter_limit' => 10000,
      'filter_sort_column' => 'nb_visits',
      'filter_sort_order' => 'desc',
    ];

    // Get analytics per page.
    $result = $this->request('Actions', 'getPageUrls', $params);

    // Stop here if no data was found.
    if (!$result) {
      return [];
    }

    // Create an array with the response rows values.
    $data = [];
    foreach ($result as $row) {
      // Make sure the path starts with a slash.
      $path = '/' . ltrim($row->label, '/');

      // Convert the "/index" path to "/" since matomo stores the homepage like
      // that.
      if ('/index' === $path) {
        $path = '/';
      }

      // Skip the path if it already exists.
      if (isset($data[$path])) {
        continue;
      }

      // Add the analytics for the page.
      $page_views = new AnalyticsPerPage();
      $page_views->setPath($path);
      $page_views->setVisits((int) $row->nb_visits);
      $data[$path] = $page_views;
    }

    return $data;
  }

  /**
   * Executes an API request.
   *
   * @param string $module
   *   The module to get the data from.
   * @param string $report
   *   The report to get the data from.
   * @param array $params
   *   Additional or altered query parameters.
   *
   * @see https://developer.matomo.org/api-reference/reporting-api
   *
   * @return mixed
   *   The API response (array or object), NULL for an empty response or FALSE
   *   when an error occurred.
   */
  protected function request(string $module, string $report, array $params = []): mixed {
    // Execute API query and return the result.
    try {
      $query = $this->matomoQueryFactory->getQuery($module . '.' . $report);
      $query->setParameters($params);
      $response = $query->execute()->getResponse();
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }

    // When the response is an empty array this process can be aborted.
    if ([] === $response) {
      return NULL;
    }

    // When the response is an error, log the message and abort this process.
    if (is_object($response) && property_exists($response, 'result') && 'error' === $response->result) {
      if (property_exists($response, 'message')) {
        $this->logger->error($response->message);
      }
      return FALSE;
    }

    return $response;
  }

}
