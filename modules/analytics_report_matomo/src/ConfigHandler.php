<?php

namespace Drupal\analytics_report_matomo;

use Drupal\analytics_report_matomo\Plugin\AnalyticsReport\Matomo;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;

/**
 * Handles configuration for the Matomo Reporting API module.
 *
 * This module uses the settings from the Matomo module so that a site builder
 * doesn't need to configure the same settings twice.
 */
class ConfigHandler implements ConfigHandlerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected Settings $settings;

  /**
   * Constructs a ConfigHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Site\Settings|null $settings
   *   The site settings.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Settings $settings = NULL) {
    $this->configFactory = $config_factory;
    $this->settings = $settings ?: Settings::getInstance();
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationToken(): ?string {
    return $this->settings->get(Matomo::MATOMO_API_AUTHENTICATION_PARAMETER_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteId(): ?string {
    return $this->getSharedData('site_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): ?string {
    return $this->getSharedData('url_https') ?: $this->getSharedData('url_http');
  }

  /**
   * Returns the configuration for the given config key.
   *
   * This will return configuration from the Matomo module.
   *
   * @param string $key
   *   The config key for which to return the configured value.
   *
   * @return string|null
   *   The configuration value, or NULL if this has not been configured yet.
   */
  protected function getSharedData(string $key): ?string {
    return $this->configFactory->get('matomo.settings')->get($key);
  }

}
