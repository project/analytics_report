<?php

namespace Drupal\analytics_report_google\Plugin\AnalyticsReport;

use Drupal\analytics_report\Objects\AnalyticsDevices;
use Drupal\analytics_report\Objects\AnalyticsGraph;
use Drupal\analytics_report\Objects\AnalyticsLive;
use Drupal\analytics_report\Objects\AnalyticsOrigin;
use Drupal\analytics_report\Objects\AnalyticsPerPage;
use Drupal\analytics_report\Objects\AnalyticsSummary;
use Drupal\analytics_report\Plugin\AnalyticsReport\AnalyticsReportBase;
use Drupal\analytics_report_google\GoogleQueryFactory;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides 'Google' analytics report.
 *
 * @AnalyticsReport(
 *   id = "google",
 *   name = @Translation("Google"),
 *   description = @Translation("Make sure the configuration for Google Analytics module is provided (/admin/config/services/google-analytics) and API key is added to settings.php before enabling this plugin."),
 * )
 */
class Google extends AnalyticsReportBase {

  /**
   * The name of the authentication token for the plugin.
   */
  const GOOGLE_API_AUTHENTICATION_PARAMETER_NAME = 'analytics_report_google_property_id';

  /**
   * The Google query factory.
   *
   * @var \Drupal\analytics_report_google\GoogleQueryFactory
   */
  protected GoogleQueryFactory $googleQueryFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    /** @var \Drupal\analytics_report_google\Plugin\AnalyticsReport\Google $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->pathAliasManager = $container->get('path_alias.manager');
    $instance->logger = $container->get('logger.factory')->get('analytics_report');
    $instance->googleQueryFactory = $container->get('analytics_report_google.query_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredConfigAndSettings(): array {
    return [
      'config' => [
        'google_analytics.settings' => ['account'],
      ],
      'settings' => [self::GOOGLE_API_AUTHENTICATION_PARAMETER_NAME],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDevices(?string $url = NULL): ?AnalyticsDevices {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:devices' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // The type of device: Desktop, Tablet, or Mobile.
    $this->googleQueryFactory->addDimension('deviceCategory');

    // The number of distinct users who visited your site or app.
    $this->googleQueryFactory->addMetric('totalUsers');

    // Set the required date range.
    $datetime = DrupalDateTime::createFromTimestamp(strtotime('-1 year'));
    $datetime->setTimezone(new \DateTimeZone('UTC'));
    $this->googleQueryFactory->addDateRange($datetime->format('Y-m-d'));

    // Add URL parameter if provided.
    if ($url) {
      $this->googleQueryFactory->addDimensionFilter('pagePath', parse_url($url, PHP_URL_PATH));
    }

    // Get the data from the API.
    $response = $this->request();

    // Stop here if something went wrong or there are no results from the API.
    if (FALSE === $response) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsDevices();
    $supported_devices = ['desktop', 'mobile', 'tablet'];
    foreach ($response->getRows() as $row) {
      $dimension_value = $row->getDimensionValues()[0];
      $metric_value = $row->getMetricValues()[0];
      if (in_array($dimension_value->getValue(), $supported_devices, TRUE)) {
        $data->setDevice($dimension_value->getValue(), $metric_value->getValue());
      }
    }

    // Add the data to the cache. The device data can be cached for 24 hours.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (24 * 60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getGraph(string $period, ?string $url = NULL): ?AnalyticsGraph {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:graph:' . $period . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Get the results per date.
    $this->googleQueryFactory->addDimension('date');

    // The number of distinct users who visited your site or app.
    $this->googleQueryFactory->addMetric('totalUsers');

    // Set the required date range.
    $timezone = new \DateTimeZone('UTC');
    $eight_days_ago = DrupalDateTime::createFromTimestamp(strtotime('-7 days'), $timezone)->setTime(12, 0);
    $today = DrupalDateTime::createFromTimestamp(time(), $timezone)->setTime(12, 0);
    $this->googleQueryFactory->addDateRange($eight_days_ago->format('Y-m-d'), $today->format('Y-m-d'));

    // Set the field and order to sort by.
    $this->googleQueryFactory->addOrderBy('dimension', 'date', FALSE);

    // Add URL parameter if provided.
    if ($url) {
      $this->googleQueryFactory->addDimensionFilter('pagePath', parse_url($url, PHP_URL_PATH));
    }

    // Get the data from the API.
    $response = $this->request();

    // Stop here if something went wrong or there are no results from the API.
    if (FALSE === $response) {
      return NULL;
    }

    // Create an array with the response rows values.
    $rows = [];
    foreach ($response->getRows() as $row) {
      $dimension_value = $row->getDimensionValues()[0];
      $metric_value = $row->getMetricValues()[0];
      $datetime = DrupalDateTime::createFromFormat('Ymd', $dimension_value->getValue(), $timezone)->setTime(12, 0);
      $rows[$datetime->getTimestamp()] = $metric_value->getValue();
    }

    // Generate dataset. Google does not return the days when the value is 0, so
    // we need to add that days ourselves.
    $data = new AnalyticsGraph();
    $time = $eight_days_ago->getTimestamp();
    while ($time < $today->getTimestamp()) {
      $visitors = $rows[$time] ?? 0;
      $data->setDateVisitors($time, $visitors);
      $time = $time + (60 * 60 * 24);
    }

    // Add the data to the cache. The graph data can be cached for 24 hours.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (24 * 60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getLive(?string $url = NULL): ?AnalyticsLive {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:live' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // Set the required metrics.
    $this->googleQueryFactory->addMetric('activeUsers');

    // Add URL parameter if provided.
    // @todo Add support for filtering on URL when the analytics API supports
    // it.
    // @see https://issuetracker.google.com/issues/235416504#comment1
    //if ($url) {
    // $this->googleQueryFactory->addDimensionFilter('pagePath', parse_url($url, PHP_URL_PATH));
    //}

    // Get the number of currently active users.
    $response = $this->request('realtime');

    // Stop here if something went wrong.
    if (FALSE === $response) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsLive();

    // Set the number of visitors.
    if (0 === $response->getRowCount()) {
      $data->setVisitors(0);
    }
    else {
      // Since we only requested a single metric we can assume there is only
      // one result row, and it has the data we need.
      $row = $response->getRows()[0];
      $data->setVisitors($row->getMetricValues()[0]->getValue());
    }

    // Events appear in realtime reports seconds after they have been sent to
    // the Google Analytics. Realtime reports show events and usage data for
    // the periods of time ranging from the present moment to 30 minutes ago.
    // @see https://developers.google.com/analytics/devguides/reporting/data/v1/realtime-api-schema
    $data->setMinutes(30);

    // Add the data to the cache. The live data can be cached for 30 minutes.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (30 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrigin(?string $url = NULL): ?AnalyticsOrigin {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:origin' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // The visitors country.
    $this->googleQueryFactory->addDimension('country');

    // The number of distinct users who visited your site or app.
    $this->googleQueryFactory->addMetric('totalUsers');

    // Set the required date range.
    $datetime = DrupalDateTime::createFromTimestamp(strtotime('-1 year'));
    $datetime->setTimezone(new \DateTimeZone('UTC'));
    $this->googleQueryFactory->addDateRange($datetime->format('Y-m-d'));

    // Set the field and order to sort by.
    $this->googleQueryFactory->addOrderBy('metric', 'totalUsers');

    // Limit the amount of results.
    $this->googleQueryFactory->addLimit(6);

    // Add URL parameter if provided.
    if ($url) {
      $this->googleQueryFactory->addDimensionFilter('pagePath', parse_url($url, PHP_URL_PATH));
    }

    // Get the data from the API.
    $response = $this->request();

    // Stop here if something went wrong or there are no results from the API.
    if (FALSE === $response) {
      return NULL;
    }

    // Generate dataset.
    $data = new AnalyticsOrigin();
    foreach ($response->getRows() as $row) {
      $dimension_value = $row->getDimensionValues()[0];
      $metric_value = $row->getMetricValues()[0];
      $data->setCountry($dimension_value->getValue(), $metric_value->getValue());
    }

    // Add the data to the cache. The origin data can be cached for 1 hour.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(?string $url = NULL): ?AnalyticsSummary {
    // Try to fetch the data from the cache.
    $cache_id = 'analytics_report:summary' . ($url ? ':' . md5($url) : '');
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    // The number of distinct users who visited your site or app.
    $this->googleQueryFactory->addMetric('totalUsers');

    // The number of users who interacted with your site or launched your app
    // for the first time.
    $this->googleQueryFactory->addMetric('newUsers');

    // The number of app screens or web pages your users viewed. Repeated views
    // of a single page or screen are counted.
    $this->googleQueryFactory->addMetric('screenPageViews');

    // The number of web pages your users viewed per session.
    $this->googleQueryFactory->addMetric('screenPageViewsPerSession');

    // The average duration (in seconds) of users' sessions.
    $this->googleQueryFactory->addMetric('averageSessionDuration');

    // The percentage of sessions that were not engaged
    // ((Sessions Minus Engaged sessions) divided by Sessions).
    $this->googleQueryFactory->addMetric('bounceRate');

    // Set the required date range.
    $datetime = DrupalDateTime::createFromTimestamp(strtotime('-1 year'));
    $datetime->setTimezone(new \DateTimeZone('UTC'));
    $this->googleQueryFactory->addDateRange($datetime->format('Y-m-d'));

    // Add URL parameter if provided.
    if ($url) {
      $this->googleQueryFactory->addDimensionFilter('pagePath', parse_url($url, PHP_URL_PATH));
    }

    // Get the data from the API.
    $response = $this->request();

    // Stop here if something went wrong or there are no results from the API.
    if (FALSE === $response || 0 === $response->getRowCount()) {
      return NULL;
    }

    // Generate dataset.
    $row = $response->getRows()[0];
    $data = new AnalyticsSummary();
    $data->setVisitors($row->getMetricValues()[0]->getValue() ?? 0);
    $data->setVisitorsNew($row->getMetricValues()[1]->getValue() ?? 0);
    $data->setPageviews($row->getMetricValues()[2]->getValue() ?? 0);
    // The screenPageViewsPerSession is returned with a lot of decimals.
    $pageviews_per_visit = 0;
    if (isset($row->getMetricValues()[3])) {
      $pageviews_per_visit = round($row->getMetricValues()[3]->getValue(), 1);
    }
    $data->setPageviewsPerVisit($pageviews_per_visit);
    $data->setVisitAvgTime((int) $row->getMetricValues()[4]->getValue());
    // The bounceRate metric is returned as a fraction; 0.2761 means 27.61% of
    // sessions were bounced.
    $bounce_rate = 0;
    if (isset($row->getMetricValues()[5])) {
      $bounce_rate = $row->getMetricValues()[5]->getValue() * 100;
    }
    $data->setBounceRate(round($bounce_rate) . '%');

    // Add the data to the cache. The summary data can be cached for 1 hour.
    $this->cache->set($cache_id, $data, $this->time->getRequestTime() + (60 * 60));

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageViews(string $period): array {
    // Get the results per path.
    $this->googleQueryFactory->addDimension('pagePath');

    // The number of distinct users who visited each page.
    $this->googleQueryFactory->addMetric('totalUsers');

    // Set the required date range.
    $timezone = new \DateTimeZone('UTC');
    $eight_days_ago = DrupalDateTime::createFromTimestamp(strtotime('-8 days'), $timezone)->setTime(12, 0);
    $today = DrupalDateTime::createFromTimestamp(time(), $timezone)->setTime(12, 0);
    $this->googleQueryFactory->addDateRange($eight_days_ago->format('Y-m-d'), $today->format('Y-m-d'));

    // Set the field and order to sort by.
    $this->googleQueryFactory->addOrderBy('metric', 'totalUsers', TRUE);

    // Get the data from the API.
    $response = $this->request();

    // Stop here if something went wrong or there are no results from the API.
    if (FALSE === $response || 0 === $response->getRowCount()) {
      return [];
    }

    // Create an array with the response rows values.
    $data = [];
    foreach ($response->getRows() as $row) {
      $page_views = new AnalyticsPerPage();
      $page_views->setPath($row->getDimensionValues()[0]->getValue());
      $page_views->setVisits((int) $row->getMetricValues()[0]->getValue());
      $data[] = $page_views;
    }

    return $data;
  }

  /**
   * Executes an API request.
   *
   * @param string $type
   *   The type of request to make. Options are 'report" and 'realtime'.
   *
   * @see https://developers.google.com/analytics/devguides/reporting/data/v1/api-schema
   * @see https://developers.google.com/analytics/devguides/reporting/data/v1/realtime-api-schema
   *
   * @return mixed
   *   The API response, NULL for an empty response or FALSE when an error
   *   occurred.
   */
  protected function request(string $type = 'report'): mixed {
    // Initialize the report parameters.
    $this->googleQueryFactory->setDefaultParameters();

    // Get all parameters.
    $report_parameters = $this->googleQueryFactory->getParameters();

    // Reset the parameters.
    $this->googleQueryFactory->setParameters([]);

    // Execute API query and return the result.
    try {
      $client = $this->googleQueryFactory->getClient();
      if ('realtime' === $type) {
        $response = $client->runRealtimeReport($report_parameters);
      }
      else {
        $response = $client->runReport($report_parameters);
      }
    }
    catch (\Exception $exception) {
      // When the response is an error, log the message and abort this process.
      $this->logger->error($exception->getMessage());
      return FALSE;
    }

    return $response;
  }

}
