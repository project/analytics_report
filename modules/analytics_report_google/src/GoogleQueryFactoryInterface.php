<?php

namespace Drupal\analytics_report_google;

use Google\Analytics\Data\V1beta\BetaAnalyticsDataClient;

/**
 * Interface for factory classes that return Google query objects.
 */
interface GoogleQueryFactoryInterface {

  /**
   * Returns a Google query object for the given method.
   *
   * @return \Google\Analytics\Data\V1beta\BetaAnalyticsDataClient
   *   The Google Analytics Data API query object.
   */
  public function getClient(): BetaAnalyticsDataClient;

}
