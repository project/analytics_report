<?php

namespace Drupal\analytics_report_google;

/**
 * Handles configuration for the Google Reporting API module.
 *
 * This module uses the settings from the Google Analytics  module so that a
 * site builder doesn't need to configure the same settings twice.
 */
interface ConfigHandlerInterface {

  /**
   * Returns the Google Analytics property ID.
   *
   * @return string|null
   *   The Google Analytics property ID, or NULL if no property ID has been
   *   configured yet.
   */
  public function getPropertyId(): ?string;

}
