<?php

namespace Drupal\analytics_report_google;

use Drupal\analytics_report_google\Plugin\AnalyticsReport\Google;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;

/**
 * Handles configuration for the Google Reporting API module.
 *
 * This module uses the settings from the Google Analytics module so that a
 * site builder doesn't need to configure the same settings twice.
 */
class ConfigHandler implements ConfigHandlerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected Settings $settings;

  /**
   * Constructs a ConfigHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Site\Settings|null $settings
   *   The site settings.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Settings $settings = NULL) {
    $this->configFactory = $config_factory;
    $this->settings = $settings ?: Settings::getInstance();
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyId(): ?string {
    return $this->settings->get(Google::GOOGLE_API_AUTHENTICATION_PARAMETER_NAME);
  }

  /**
   * Returns the configuration for the given config key.
   *
   * This will return configuration from the Google Analytics module.
   *
   * @param string $key
   *   The config key for which to return the configured value.
   *
   * @return string|null
   *   The configuration value, or NULL if this has not been configured yet.
   */
  protected function getSharedData(string $key): ?string {
    return $this->configFactory->get('google_analytics.settings')->get($key);
  }

}
