<?php

namespace Drupal\analytics_report_google;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Google\Analytics\Data\V1beta\BetaAnalyticsDataClient;
use Google\Analytics\Data\V1beta\DateRange;
use Google\Analytics\Data\V1beta\Dimension;
use Google\Analytics\Data\V1beta\Filter;
use Google\Analytics\Data\V1beta\Filter\StringFilter;
use Google\Analytics\Data\V1beta\FilterExpression;
use Google\Analytics\Data\V1beta\Metric;
use Google\Analytics\Data\V1beta\OrderBy;
use Google\Analytics\Data\V1beta\OrderBy\DimensionOrderBy;
use Google\Analytics\Data\V1beta\OrderBy\MetricOrderBy;

/**
 * Factory for Google query objects.
 *
 * This wraps the beta analytics data client from query factory from the Google
 * analytics-data library.
 *
 * @see \Google\Analytics\Data\V1beta\Gapic\BetaAnalyticsDataGapicClient
 */
class GoogleQueryFactory implements GoogleQueryFactoryInterface {

  /**
   * The config handler.
   *
   * @var \Drupal\analytics_report_google\ConfigHandlerInterface
   */
  protected ConfigHandlerInterface $configHandler;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * The client from the beta analytics data client.
   *
   * @var \Google\Analytics\Data\V1beta\BetaAnalyticsDataClient
   */
  protected BetaAnalyticsDataClient $client;

  /**
   * All parameters for the API call.
   *
   * @var array
   */
  protected array $parameters;

  /**
   * Constructs a new GoogleQueryFactory.
   *
   * @param \Drupal\analytics_report_google\ConfigHandlerInterface $configHandler
   *   The config handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(ConfigHandlerInterface $configHandler, LoggerChannelFactoryInterface $loggerFactory) {
    $this->configHandler = $configHandler;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(): BetaAnalyticsDataClient {
    if (empty($this->client)) {
      $this->client = $this->createFactoryInstance();
    }

    return $this->client;
  }

  /**
   * Generates and returns a new instance of the factory, with defaults applied.
   *
   * @return \Google\Analytics\Data\V1beta\BetaAnalyticsDataClient
   *   The Google Analytics data client.
   */
  protected function createFactoryInstance(): BetaAnalyticsDataClient {
    $credentials_path = DRUPAL_ROOT . '/../settings/google_analytics_credentials.json';
    putenv("GOOGLE_APPLICATION_CREDENTIALS={$credentials_path}");

    return new BetaAnalyticsDataClient();
  }

  /**
   * Get all query parameters.
   *
   * @return array
   *   All query parameters.
   */
  public function getParameters(): array {
    return $this->parameters;
  }

  /**
   * Set all query parameters.
   *
   * @param array $parameters
   *   All query parameters.
   */
  public function setParameters(array $parameters): void {
    $this->parameters = $parameters;
  }

  /**
   * Retrieves default parameters from config.
   */
  public function setDefaultParameters(): void {
    $parameters = $this->getParameters();

    // The Google property ID.
    if ($property = $this->configHandler->getPropertyId()) {
      $parameters['property'] = 'properties/' . $property;
    }

    // Make sure we also include rows with no data.
    $parameters['keepEmptyRows'] = TRUE;

    $this->setParameters($parameters);
  }

  /**
   * Add a new date range to the parameters.
   *
   * @param string $start_date
   *   The start date in format yyyy-mm-dd.
   * @param string $end_date
   *   The end date in format yyyy-mm-dd.
   */
  public function addDateRange(string $start_date, string $end_date = 'today') {
    $this->addParameterToBundle('dateRanges', new DateRange([
      'start_date' => $start_date,
      'end_date' => $end_date,
    ]));
  }

  /**
   * Add a new dimension to the parameters.
   *
   * @param string $name
   *   The name of the dimension.
   */
  public function addDimension(string $name) {
    $this->addParameterToBundle('dimensions', new Dimension(['name' => $name]));
  }

  /**
   * Add a new order by to the parameters.
   *
   * @param string $name
   *   The name of the dimension to filter on.
   * @param string $value
   *   The value to filter on.
   */
  public function addDimensionFilter(string $name, string $value) {
    $this->parameters['dimensionFilter'] = new FilterExpression([
      'filter' => new Filter([
        'field_name' => $name,
        'string_filter' => new StringFilter([
          'match_type' => 1,
          'value' => $value,
        ]),
      ]),
    ]);
  }

  /**
   * Add a limit to the parameters.
   *
   * @param int $limit
   *   The max amount of results.
   */
  public function addLimit(int $limit) {
    $this->parameters['limit'] = $limit;
  }

  /**
   * Add a new metric to the parameters.
   *
   * @param string $name
   *   The name of the metric.
   */
  public function addMetric(string $name) {
    $this->addParameterToBundle('metrics', new Metric(['name' => $name]));
  }

  /**
   * Add a new order by to the parameters.
   *
   * @param string $type
   *   The type of parameter to sort by. This can be 'dimension' or 'metric',
   *   defaults to 'metric'.
   * @param string $name
   *   The name of the parameter type to sort by.
   * @param bool $desc
   *   Sort in descending order, or not.
   */
  public function addOrderBy(string $type, string $name, bool $desc = TRUE) {
    $order_by = new OrderBy();
    if ('dimension' === $type) {
      $order_by->setDimension(new DimensionOrderBy([
        'dimension_name' => $name,
      ]));
    }
    else {
      $order_by->setMetric(new MetricOrderBy([
        'metric_name' => $name,
      ]));
    }
    $order_by->setDesc($desc);

    $this->addParameterToBundle('orderBys', $order_by);
  }

  /**
   * Set a query parameter to a bundle.
   *
   * @param string $bundle
   *   The name of the bundle to add the parameter to.
   * @param mixed $parameter
   *   The value for the parameter.
   */
  public function addParameterToBundle(string $bundle, $parameter): void {
    $this->parameters[$bundle][] = $parameter;
  }

}
