<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics per page data.
 */
interface AnalyticsPerPageInterface {

  /**
   * Gets the path of the page.
   *
   * @return string
   *   The path of the page.
   */
  public function getPath(): string;

  /**
   * Sets the path of the page.
   *
   * @param string $path
   *   The path of the page.
   */
  public function setPath(string $path): void;

  /**
   * Gets the number of visits per page.
   *
   * @return int
   *   The number of visits per page.
   */
  public function getVisits(): int;

  /**
   * Sets the number of visits per page.
   *
   * @param int $visits_per_page
   *   The number of visits per page.
   */
  public function setVisits(int $visits_per_page): void;

}
