<?php

namespace Drupal\analytics_report\Objects;

/**
 * Provides an interface defining the value object to store live data.
 */
interface AnalyticsLiveInterface {

  /**
   * Sets the number of live actions.
   *
   * @return int|null
   *   The number of live actions.
   */
  public function getActions(): ?int;

  /**
   * Gets the number of live actions.
   *
   * @param int $actions
   *   The number of live actions.
   */
  public function setActions(int $actions): void;

  /**
   * Sets the number of minutes.
   *
   * @return int|null
   *   The number of minutes.
   */
  public function getMinutes(): ?int;

  /**
   * Gets the number of minutes.
   *
   * @param int $minutes
   *   The number of minutes.
   */
  public function setMinutes(int $minutes): void;

  /**
   * Sets the number of live visitors.
   *
   * @return int|null
   *   The number of live visitors.
   */
  public function getVisitors(): ?int;

  /**
   * Gets the number of live visitors.
   *
   * @param int $visitors
   *   The number of live visitors.
   */
  public function setVisitors(int $visitors): void;

}
