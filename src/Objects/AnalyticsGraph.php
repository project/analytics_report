<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics periodic data.
 */
class AnalyticsGraph implements AnalyticsGraphInterface {

  /**
   * The set of periodic data.
   *
   * @var array
   */
  protected array $graphData = [];

  /**
   * {@inheritdoc}
   */
  public function getGraphData(): array {
    return $this->graphData;
  }

  /**
   * {@inheritdoc}
   */
  public function setDateVisitors(int $timestamp, int $visitors): void {
    $this->graphData[$timestamp] = $visitors;
  }

}
