<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics devices data.
 */
interface AnalyticsDevicesInterface {

  /**
   * Gets the device data.
   *
   * @return array|null
   *   The number of visits keyed by device type.
   */
  public function getDevices(): ?array;

  /**
   * Sets the device data.
   *
   * @param string $label
   *   The device type label.
   * @param int $visits
   *   The amount of visits for the device type.
   */
  public function setDevice(string $label, int $visits): void;

}
