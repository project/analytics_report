<?php

namespace Drupal\analytics_report\Objects;

/**
 * Provides an interface defining the value object to store periodic data.
 */
interface AnalyticsGraphInterface {

  /**
   * Gets the set with periodic data.
   *
   * @return array
   *   The set with periodic data.
   */
  public function getGraphData(): array;

  /**
   * Adds a date to the list of graph data.
   *
   * @param int $timestamp
   *   The date timestamp.
   * @param int $visitors
   *   The number of visitors.
   */
  public function setDateVisitors(int $timestamp, int $visitors): void;

}
