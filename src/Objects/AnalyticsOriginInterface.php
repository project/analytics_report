<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics origin data.
 */
interface AnalyticsOriginInterface {

  /**
   * Gets the country data.
   *
   * @return array|null
   *   The number of visits keyed by country.
   */
  public function getCountries(): ?array;

  /**
   * Sets the country data.
   *
   * @param string $label
   *   The country label.
   * @param int $visits
   *   The amount of visits for the country.
   */
  public function setCountry(string $label, int $visits): void;

}
