<?php

namespace Drupal\analytics_report\Objects;

/**
 * Provides an interface defining the value object to store summary data.
 */
interface AnalyticsSummaryInterface {

  /**
   * Gets the bounce rate.
   *
   * @return string|null
   *   The bounce rate.
   */
  public function getBounceRate(): ?string;

  /**
   * Sets the bounce rate.
   *
   * @param string $bounce_rate
   *   The bounce rate.
   */
  public function setBounceRate(string $bounce_rate): void;

  /**
   * Get the amount of pageviews.
   *
   * @return int|null
   *   The amount of pageviews.
   */
  public function getPageviews(): ?int;

  /**
   * Sets the amount of pageviews.
   *
   * @param float|int $pageviews
   *   The amount of pageviews.
   */
  public function setPageviews(float|int $pageviews): void;

  /**
   * Gets the amount of pageviews per visit.
   *
   * @return float|int|null
   *   The amount pageviews per visit.
   */
  public function getPageviewsPerVisit(): float|int|NULL;

  /**
   * Sets the amount pageviews per visit.
   *
   * @param int $pageviews
   *   The amount of pageviews per visit.
   */
  public function setPageviewsPerVisit(int $pageviews): void;

  /**
   * Gets the average visit time.
   *
   * @return int|null
   *   The average visit time.
   */
  public function getVisitAvgTime(): ?int;

  /**
   * Sets the average visit time.
   *
   * @param int $visit_avg_time
   *   The average visit time.
   */
  public function setVisitAvgTime(int $visit_avg_time): void;

  /**
   * Gets the amount of visits.
   *
   * @return int|null
   *   The amount of visits.
   */
  public function getVisitors(): ?int;

  /**
   * Sets the amount of visits.
   *
   * @param int $visitors
   *   The amount of visits.
   */
  public function setVisitors(int $visitors): void;

  /**
   * Gets the amount of new visitors.
   *
   * @return int|null
   *   The amount of new visitors.
   */
  public function getVisitorsNew(): ?int;

  /**
   * Sets the amount of new visitors.
   *
   * @param int $visitors_new
   *   The amount of new visitors.
   */
  public function setVisitorsNew(int $visitors_new): void;

}
