<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics origin data.
 */
class AnalyticsOrigin implements AnalyticsOriginInterface {

  /**
   * The set of browser data.
   *
   * @var array
   */
  protected array $countries = [];

  /**
   * {@inheritdoc}
   */
  public function getCountries(): ?array {
    return $this->countries ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setCountry(string $label, int $visits): void {
    $this->countries[] = [
      'label' => $label,
      'visits' => $visits,
    ];
  }

}
