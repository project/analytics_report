<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics live data.
 */
class AnalyticsLive implements AnalyticsLiveInterface {

  /**
   * The number of live actions.
   *
   * @var int
   */
  protected int $actions = 0;

  /**
   * The number of minutes.
   *
   * @var int
   */
  protected int $minutes = 0;

  /**
   * The number of live visitors.
   *
   * @var int
   */
  protected int $visitors = 0;

  /**
   * {@inheritdoc}
   */
  public function getActions(): int {
    return $this->actions;
  }

  /**
   * {@inheritdoc}
   */
  public function setActions(int $actions): void {
    $this->actions = $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function getMinutes(): int {
    return $this->minutes;
  }

  /**
   * {@inheritdoc}
   */
  public function setMinutes(int $minutes): void {
    $this->minutes = $minutes;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisitors(): int {
    return $this->visitors;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisitors(int $visitors): void {
    $this->visitors = $visitors;
  }

}
