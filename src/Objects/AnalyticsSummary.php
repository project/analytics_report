<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics summary data.
 */
class AnalyticsSummary implements AnalyticsSummaryInterface {

  /**
   * The bounce rate.
   *
   * @var string
   */
  protected string $bounceRate = '0%';

  /**
   * The amount of pageviews.
   *
   * @var int
   */
  protected int $pageviews = 0;

  /**
   * The amount of actions per visit.
   *
   * @var float|int
   */
  protected float|int $pageviewsPerVisit = 0;

  /**
   * The average visit time.
   *
   * @var int
   */
  protected int $visitAvgTime = 0;

  /**
   * The amount of visitors.
   *
   * @var int
   */
  protected int $visitors = 0;

  /**
   * The amount of new visitors.
   *
   * @var int
   */
  protected int $visitorsNew = 0;

  /**
   * {@inheritdoc}
   */
  public function getBounceRate(): ?string {
    return $this->bounceRate ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setBounceRate($bounce_rate): void {
    $this->bounceRate = $bounce_rate;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageviews(): ?int {
    return $this->pageviews ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setPageviews(float|int $pageviews): void {
    $this->pageviews = $pageviews;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageviewsPerVisit(): float|int|NULL {
    return $this->pageviewsPerVisit ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setPageviewsPerVisit(float|int $pageviews): void {
    $this->pageviewsPerVisit = $pageviews;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisitAvgTime(): ?int {
    return $this->visitAvgTime ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisitAvgTime(int $visit_avg_time): void {
    $this->visitAvgTime = $visit_avg_time;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisitors(): ?int {
    return $this->visitors ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisitors(int $visitors): void {
    $this->visitors = $visitors;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisitorsNew(): ?int {
    return $this->visitorsNew ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisitorsNew(int $visitors_new): void {
    $this->visitorsNew = $visitors_new;
  }

}
