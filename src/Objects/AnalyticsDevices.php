<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics devices data.
 */
class AnalyticsDevices implements AnalyticsDevicesInterface {

  /**
   * The set of browser data.
   *
   * @var array
   */
  protected array $deviceData = [];

  /**
   * {@inheritdoc}
   */
  public function getDevices(): ?array {
    return $this->deviceData ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setDevice(string $label, int $visits): void {
    $this->deviceData[] = [
      'label' => $label,
      'visits' => $visits,
    ];
  }

}
