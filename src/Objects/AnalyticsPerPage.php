<?php

namespace Drupal\analytics_report\Objects;

/**
 * A value object to store analytics page visit data.
 */
class AnalyticsPerPage implements AnalyticsPerPageInterface {

  /**
   * The path of the page.
   *
   * @var string
   */
  protected string $path = '';

  /**
   * The number of visits.
   *
   * @var int
   */
  protected int $visits = 0;

  /**
   * {@inheritdoc}
   */
  public function getPath(): string {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath(string $path): void {
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisits(): int {
    return $this->visits;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisits(int $visits_per_page): void {
    $this->visits = $visits_per_page;
  }

}
