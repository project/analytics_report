<?php

namespace Drupal\analytics_report\Form;

use Drupal\analytics_report\Plugin\AnalyticsReportPluginManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the analytics report module.
 */
class AnalyticsReportSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The analytics report plugin manager.
   *
   * @var \Drupal\analytics_report\Plugin\AnalyticsReportPluginManager
   */
  protected AnalyticsReportPluginManager $analyticsReportPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->analyticsReportPluginManager = $container->get('plugin.manager.analytics_report');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'analytics_report_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [AnalyticsReportPluginManager::ANALYTICS_REPORT_CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(AnalyticsReportPluginManager::ANALYTICS_REPORT_CONFIG_NAME);
    $definitions = $this->analyticsReportPluginManager->getDefinitions();

    // Add plugins list to form.
    $form['enabled_plugin'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose an analytics provider.'),
      '#options' => [],
      '#default_value' => $config->get('enabled_plugin'),
      '#required' => !empty($definitions),
    ];

    // Add enabled plugin options.
    ksort($definitions);
    foreach ($definitions as $plugin_id => $plugin_definition) {
      $form['enabled_plugin']['#options'][$plugin_id] = $plugin_definition['name'] ?? ucfirst($plugin_id);
      $form['enabled_plugin'][$plugin_id]['#description'] = $plugin_definition['description'];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Store the enabled plugin.
    $this->config(AnalyticsReportPluginManager::ANALYTICS_REPORT_CONFIG_NAME)
      ->set('enabled_plugin', $form_state->getValue('enabled_plugin'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
