<?php

namespace Drupal\analytics_report\Plugin;

use Drupal\analytics_report\Objects\AnalyticsDevices;
use Drupal\analytics_report\Objects\AnalyticsGraph;
use Drupal\analytics_report\Objects\AnalyticsLive;
use Drupal\analytics_report\Objects\AnalyticsOrigin;
use Drupal\analytics_report\Objects\AnalyticsSummary;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface defining analytics report plugins.
 */
interface AnalyticsReportInterface extends PluginInspectionInterface {

  /**
   * Get the required configuration and settings items.
   *
   * @return array
   *   The required configuration and settings items.
   */
  public function getRequiredConfigAndSettings(): array;

  /**
   * Retrieves devices dataset from the analytics provider.
   *
   * @param string|null $url
   *   (optional) The url to retrieve the devices for. If not provided, the
   *   result is aggregated for all URLs.
   *
   * @return \Drupal\analytics_report\Objects\AnalyticsDevices|null
   *   The website analytics devices dataset.
   */
  public function getDevices(?string $url = NULL): ?AnalyticsDevices;

  /**
   * Retrieves a periodic dataset from the analytics provider.
   *
   * @param string $period
   *   The period to retrieve.
   * @param string|null $url
   *   (optional) The url to retrieve the graph for. If not provided, the result
   *   is aggregated for all URLs.
   *
   * @return \Drupal\analytics_report\Objects\AnalyticsGraph|null
   *   A website analytics periodic dataset.
   */
  public function getGraph(string $period, ?string $url = NULL): ?AnalyticsGraph;

  /**
   * Retrieves the live dataset from the analytics provider.
   *
   * @param string|null $url
   *   (optional) The url to retrieve the live data for. If not provided, the
   *   result is aggregated for all URLs.
   *
   * @return \Drupal\analytics_report\Objects\AnalyticsLive|null
   *   The website analytics live dataset.
   */
  public function getLive(?string $url = NULL): ?AnalyticsLive;

  /**
   * Retrieves origin dataset from the analytics provider.
   *
   * @param string|null $url
   *   (optional) The url to retrieve the origin data for. If not provided, the
   *   result is aggregated for all URLs.
   *
   * @return \Drupal\analytics_report\Objects\AnalyticsOrigin|null
   *   A value object with analytics origin data.
   */
  public function getOrigin(?string $url = NULL): ?AnalyticsOrigin;

  /**
   * Retrieves the summary dataset from the analytics provider.
   *
   * @param string|null $url
   *   (optional) The url to retrieve the summary for. If not provided, the
   *   result is aggregated for all URLs.
   *
   * @return \Drupal\analytics_report\Objects\AnalyticsSummary|null
   *   The website analytics summary dataset.
   */
  public function getSummary(?string $url = NULL): ?AnalyticsSummary;

}
