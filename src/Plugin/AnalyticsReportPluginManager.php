<?php

namespace Drupal\analytics_report\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the analytics report plugin manager.
 */
class AnalyticsReportPluginManager extends DefaultPluginManager {

  /**
   * The analytics report config name.
   */
  public const ANALYTICS_REPORT_CONFIG_NAME = 'analytics_report.settings';

  /**
   * The analytics report config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs an AnalyticsReportManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/AnalyticsReport',
      $namespaces,
      $module_handler,
      'Drupal\analytics_report\Plugin\AnalyticsReportInterface',
      'Drupal\analytics_report\Annotation\AnalyticsReport'
    );
    $this->alterInfo('analytics_report_info');
    $this->setCacheBackend($cache_backend, 'analytics_report_plugins');
    $this->config = $config_factory->get(self::ANALYTICS_REPORT_CONFIG_NAME);
  }

  /**
   * Gets the active analytics plugin.
   *
   * @param string $id
   *   The ID of the plugin to retrieve. When no ID is provided an ID from the
   *   module config is used.
   *
   * @return object|null
   *   The active analytics plugin, or NULL when there is a misconfiguration.
   */
  public function getPlugin(string $id = ''): ?object {
    // Get an instance of the requested plugin.
    if ($id) {
      return $this->createInstance($id);
    }

    // Get an instance of a plugin based on the id from configuration.
    if ($id = $this->config->get('enabled_plugin')) {
      return $this->createInstance($id);
    }

    return NULL;
  }

}
