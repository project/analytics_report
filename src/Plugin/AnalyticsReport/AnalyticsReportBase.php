<?php

namespace Drupal\analytics_report\Plugin\AnalyticsReport;

use Drupal\analytics_report\Plugin\AnalyticsReportInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for analytics report plugins.
 */
abstract class AnalyticsReportBase extends PluginBase implements AnalyticsReportInterface, ContainerFactoryPluginInterface {

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected AliasManagerInterface $pathAliasManager;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected Settings $settings;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->pathAliasManager = $container->get('path_alias.manager');
    $instance->configFactory = $container->get('config.factory');
    $instance->settings = $container->get('settings')::getInstance();
    $instance->cache = $container->get('analytics_report.cache');
    $instance->time = $container->get('datetime.time');
    $instance->logger = $container->get('logger.factory')->get('analytics_report');
    return $instance;
  }

  /**
   * Get the front page url.
   *
   * @return array
   *   An array containing the front page alias and absolute url.
   */
  public function getFrontPageUrl(): array {
    $config = $this->configFactory->getEditable('system.site');
    $page_front = $config->get('page.front');
    $alias = $this->pathAliasManager->getAliasByPath($page_front);

    return [
      'absolute_url' => Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString(),
      'alias' => $alias,
    ];
  }

}
