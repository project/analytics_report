<?php

namespace Drupal\analytics_report\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines aan analytics report plugin annotation object.
 *
 * @see \Drupal\analytics_report\Plugin\AnalyticsReportPluginManager
 *
 * @Annotation
 */
class AnalyticsReport extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The name of the analytics report plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $name;

  /**
   * Description for the settings form.
   *
   * @var string
   */
  public string $descriptions;

}
